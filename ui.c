/*
 * This file is part of Resource Organizer.
 *
 * Copyright (C) 2014 Nikita Zlobin <nick87720z@gmail.com>
 *
 * Resource Organizer is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Resource Organizer is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resource Organizer.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>

#include "misc.h"
#include "ui.h"

GtkBuilder * builder;
GtkWidget * win;
GtkListStore * list;
GtkTreeModelFilter * filter;
GtkTreeModelSort * sort;
GtkTreeView * view;
GtkEntryBuffer * filterBuffer;
GtkCheckButton * ladspaCheck,
               * dssiCheck,
               * lv2Check,
               * vstCheck;

void on_win_close (void)
{
	gtk_main_quit ();
}

void column_clicked (GtkTreeViewColumn * col)
{
	static GtkTreeViewColumn * prev;
	int col_i = 0;

	{
		GList * list = gtk_tree_view_get_columns (view);
		for (GList * c = list; c && c->data != col; c = c->next, col_i++);
		g_list_free (list);
	}

	// Order change
	typeof (col->sort_order) order = (prev == col) ?
		(col->sort_order == GTK_SORT_ASCENDING ? GTK_SORT_DESCENDING : GTK_SORT_ASCENDING) :
		GTK_SORT_ASCENDING;
	gtk_tree_view_column_set_sort_order (col, order);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (sort), col_i, order);

	prev = col;
}

void view_drag_data_get (
	GtkWidget * w,
	UNUSED GdkDragContext * context,
	GtkSelectionData * sel_data,
	UNUSED int info,
	UNUSED int time,
	UNUSED void * data
){
	GtkTreeModel * mod;
	GtkTreeIter i;
	GValue path_val = G_VALUE_INIT;

	GtkTreeSelection * sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (w));
	if (! gtk_tree_selection_get_selected (sel, &mod, &i))
	{
		fprintf (stderr, "Can't drag, because nothing selected\n");
		return;
	}

	gtk_tree_model_get_value (mod, &i, COL_URL, & path_val);
	const char * path = g_value_get_string (& path_val);

	gtk_selection_data_set_text (sel_data, path, -1);
}

gboolean filter_visible (
	GtkTreeModel * model,
	GtkTreeIter  * iter,
	__attribute__((unused)) gpointer user_data
){
	gboolean ret_val = TRUE;
	char * name,
	     * type,
	     * author,
	     * file,
	     * entry_text = NULL;

	gtk_tree_model_get (model, iter,
	                    COL_NAME,      &name,
	                    COL_TYPE_NAME, &type,
	                    COL_AUTHOR,    &author,
	                    COL_FILE,      &file, -1);
	if (!name || !type || !author || !file)
		goto reject;

	if (strcmp (type, "LADSPA") == 0 && !gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ladspaCheck)))
		goto reject;

	if (strcmp (type, "DSSI") == 0 && !gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dssiCheck)))
		goto reject;

	if (strcmp (type, "LV2") == 0 && !gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (lv2Check)))
		goto reject;

	if( gtk_entry_buffer_get_length (filterBuffer))
		entry_text = (char *) gtk_entry_buffer_get_text (filterBuffer);

	if ( entry_text &&
	     (!g_strrstr( name,   entry_text) &&
	     !g_strrstr( type,   entry_text) &&
	     !g_strrstr( author, entry_text))
	)
		goto reject;

	exit:;
	free (name);
	free (type);
	free (author);
	free (file);
	return ret_val;

	reject:;
	ret_val = FALSE;
	goto exit;
}

int ui_init (int argc, char ** argv)
{
	GError * gErr = NULL;
	gtk_init (& argc, & argv);

	builder = gtk_builder_new ();
	gtk_builder_add_from_file (builder, DATADIR "/resorg/browser.ui", & gErr);
	if (gErr)
	{
		gErrorReport (& gErr);
		return -1;
	}

	list = GTK_LIST_STORE (gtk_builder_get_object (builder, "listStore"));
	filter = GTK_TREE_MODEL_FILTER (gtk_builder_get_object (builder, "listFilter"));
	sort = GTK_TREE_MODEL_SORT (gtk_builder_get_object (builder, "listSort"));
	view = GTK_TREE_VIEW (gtk_builder_get_object (builder, "treeView"));
	filterBuffer = gtk_entry_get_buffer (GTK_ENTRY (gtk_builder_get_object (builder, "filterEntry")));
	ladspaCheck = GTK_CHECK_BUTTON (gtk_builder_get_object (builder, "ladspaCheck"));
	dssiCheck = GTK_CHECK_BUTTON (gtk_builder_get_object (builder, "dssiCheck"));
	lv2Check = GTK_CHECK_BUTTON (gtk_builder_get_object (builder, "lv2Check"));
	vstCheck = GTK_CHECK_BUTTON (gtk_builder_get_object (builder, "vstCheck"));
	win = GTK_WIDGET (gtk_builder_get_object (builder, "mainWin"));

	{
		GtkTargetEntry entry = {
			.target = "STRING",
			.flags = GTK_TARGET_OTHER_APP,
			.info = 0
		};

		gtk_drag_source_set (GTK_WIDGET (view), GDK_BUTTON1_MASK, & entry, 1, GDK_ACTION_COPY);
		gtk_tree_view_enable_model_drag_source (view, GDK_BUTTON1_MASK, & entry, 1, GDK_ACTION_COPY);
	}

	gtk_tree_model_filter_set_visible_func (filter, filter_visible, NULL, NULL);
	gtk_builder_connect_signals (builder, NULL);
	gtk_tree_view_column_clicked (gtk_tree_view_get_column (view, COL_NAME));

	return 0;
}
