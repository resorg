/*
 * This file is part of Resources Organizer.
 *
 * Copyright (C) 2014 Nikita Zlobin <nick87720z@gmail.com>
 *
 * Resource Organizer is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Resource Organizer is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resource Organizer.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>

enum column
{
	COL_NAME = 0,
	COL_TYPE_NAME,
	COL_AUTHOR,
	COL_TIP,
	COL_FILE,
	COL_URL,

	COLUMNS
};

extern GtkBuilder * builder;
extern GtkWidget * win;
extern GtkListStore * list;
extern GtkTreeModelFilter * filter;
extern GtkTreeModelSort * sort;
extern GtkTreeView * view;
extern GtkEntryBuffer * filterBuffer;
extern GtkCheckButton * ladspaCheck,
               * dssiCheck,
               * lv2Check,
               * vstCheck;

extern gboolean filter_visible (
	GtkTreeModel * model,
	GtkTreeIter  * iter,
	gpointer user_data
);

extern int ui_init (int argc, char ** argv);
extern void gErrorReport (GError ** err);
