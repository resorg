/*
 * This file is part of Resources Organizer.
 *
 * Copyright (C) 2014 Nikita Zlobin <nick87720z@gmail.com>
 *
 * Resource Organizer is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Resource Organizer is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resource Organizer.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include "ladspa/search.h"
#include "dssi/search.h"
#include "lv2/search.h"

#include <resource.h>
#include <misc.h>
#include "ui.h"

#include <gtk/gtk.h>
#include <glib.h>

void gErrorReport (GError ** err)
{
	fprintf (stderr, "gtkbuilder: %s\n", (*err)->message);
	g_clear_error (err);
}

static inline
void escape_text (char ** t)
{
	char * old = *t;
	*t = g_markup_escape_text (*t, -1);
	free (old);
}

void resourceAdd (Resource * r)
{
	GtkTreeIter i, *prev_i = NULL;
	gtk_list_store_insert_after (list, & i, prev_i);
	gtk_list_store_set (list, & i,
		COL_NAME,      r->name,
		COL_TYPE_NAME, r->type->name,
		COL_AUTHOR,    r->author,
		COL_FILE,      r->file,
		COL_URL,       r->url,
	-1);
	char * tip = NULL;
	if (asprintf (& tip, "%s\n"
	                 "%s\n"
	                 "%s\n"
	                 "%s",
	              r->name,
	              r->file,
	              r->author,
	              r->type->name) == -1)
	{
		fprintf (stderr, "ERROR: Failed to asprintf() tooltip text\n");
		if (tip) free (tip);
	} else {
		escape_text (& tip);
		gtk_list_store_set (list, & i, COL_TIP, tip, -1);
		free (tip);
	}
}

static inline
void setResourceHandler (ResourceHandlerFunc cb_func)
{
	ladspaResourceHandler (cb_func);
	dssiResourceHandler (cb_func);
	lv2ResourceHandler (cb_func);
}

int main (int argc, char ** argv)
{
	if (ui_init (argc, argv) != 0) return 0;

	setResourceHandler (resourceAdd);
	ladspaPathIterate ();
	dssiPathIterate ();
	lv2PluginsList ();

	g_object_unref (G_OBJECT (builder));
	gtk_widget_show_all (win);

	gtk_main ();
	return 0;
}
