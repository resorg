/*
 * This file is part of Resource Organizer.
 *
 * Copyright (C) 2014 Nikita Zlobin <nick87720z@gmail.com>
 *
 * Resource Organizer is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Resource Organizer is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resource Organizer.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <dirent.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <search-so.h>

/* Search just the one directory. */
static void
dirIterate (
	const char * dirname,
	FilePathHandlerFunc cb_func
){
	char * fileName = NULL;
	DIR * dir;
	long lDirLength;
	long iNeedSlash;
	struct dirent * dirEntry;

	lDirLength = strlen (dirname);
	if (!lDirLength) return;
	if (dirname [lDirLength - 1] == '/')
		iNeedSlash = 0;
	else
		iNeedSlash = 1;

	dir = opendir (dirname);
	if (! dir) return;

	size_t size = 0, new_size;
	while (1) {
		dirEntry = readdir (dir);
		if (! dirEntry) {
			free (fileName);
			closedir (dir);
			return;
		}

		/* Skip special names */
		if (strcmp (dirEntry->d_name, "..") == 0 || strcmp (dirEntry->d_name, ".") == 0)
			continue;

		/* Reallocation */
		new_size = lDirLength
		           + strlen (dirEntry->d_name)
		           + 1 + iNeedSlash;

		if (size < new_size)
			fileName = realloc (fileName, (size = new_size));

		strcpy (fileName, dirname);
		if (iNeedSlash)
			strcat (fileName, "/");
		strcat (fileName, dirEntry->d_name);

		/* Recursive search */
		if (dirEntry->d_type == DT_DIR)
		{
			dirIterate (fileName, cb_func);
			continue;
		}

		/* We've successfully found a ladspa_descriptor function. Pass
		 *          it to the callback function. */
		cb_func (fileName);
	}
}

/*****************************************************************************/

void pathIterate (
	const char * pathEnv,
	FilePathHandlerFunc cb_func
){
	char * buf = NULL;
	const char * pcPath;
	const char * pcStart;
	const char * pcEnd;

	pcPath = getenv (pathEnv);
	if (!pcPath)
	{
		fprintf( stderr,
		         "Warning: You do not have a LADSPA_PATH "
		         "environment variable set.\n" );
		return;
	}

	pcEnd = pcStart = pcPath;
	while (1) {
		while (*pcEnd != ':' && *pcEnd != '\0')
			pcEnd++;

		size_t size = 0, new_size, len;
		len = pcEnd - pcStart;
		new_size = 1 + len;

		if (size < new_size)
			buf = realloc (buf, (size = new_size));

		if (pcEnd > pcStart)
			strncpy (buf, pcStart, len);
		buf [len] = '\0';

		dirIterate (buf, cb_func);

		if (* pcEnd == ':') pcStart = ++pcEnd;
		else {
			free (buf);
			break;
		}
	}
}

/*****************************************************************************/

/* EOF */
