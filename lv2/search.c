/*
 * This file is part of Resource Organizer.
 *
 * Copyright (C) 2014 Nikita Zlobin <nick87720z@gmail.com>
 *
 * Resource Organizer is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Resource Organizer is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resource Organizer.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*****************************************************************************/

#define _GNU_SOURCE

#include <dirent.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <lilv/lilv.h>

/*****************************************************************************/

#include <resource.h>
#include <search-so.h>

static ResourceType rtype = {
	.label = "lv2",
	.name = "LV2"
};

static ResourceHandlerFunc res_handler;
static int mailto_len = strlen ("mailto:");

/*****************************************************************************/

void lv2PluginsList (void)
{
	LilvWorld * world = lilv_world_new ();
	lilv_world_load_all (world);

	const LilvPlugins * plugins = lilv_world_get_all_plugins (world);

	Resource res = { .type = & rtype,
	                 .url = NULL };

	LILV_FOREACH (plugins, i, plugins) {
		LilvNode * n[] = {NULL, NULL, NULL, NULL, NULL};
		unsigned ni = 0;

		const LilvPlugin * p = lilv_plugins_get (plugins, i);

		res.file = lilv_node_as_string (lilv_plugin_get_library_uri (p));
		res.url = lilv_node_as_uri (lilv_plugin_get_uri (p));
		res.name = lilv_node_as_string (n [ni++] = lilv_plugin_get_name (p));

		char * maker = NULL, * email = NULL;
		maker = lilv_node_as_string (n [ni++] = lilv_plugin_get_author_name (p));
		email = lilv_node_as_string (n [ni++] = lilv_plugin_get_author_email (p));
		if (maker != NULL)
			if (email != NULL)
				asprintf (& res.author, "%s <%s>", maker, email + mailto_len);
			else
				res.author = strdup (maker);
		else if (email != NULL)
			res.author = strdup (email + mailto_len);
		else
			res.author = NULL;

		res_handler (& res);

		while (ni) free (n [--ni]);
	}
	lilv_world_free (world);
}

void lv2ResourceHandler (ResourceHandlerFunc cb_func)
{
	res_handler = cb_func;
}

/*****************************************************************************/

/* EOF */
