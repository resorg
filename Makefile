pkg = resorg
PREFIX = ${DESTDIR}/usr/local
BINDIR = ${PREFIX}/bin
DATADIR = ${PREFIX}/share
DOCDIR = ${DATADIR}/doc

CC=gcc
RELEASE_FLAGS = -O2
DEBUG_FLAGS = -O0 -g3 -ggdb3
COMMON_FLAGS = ${DEBUG_FLAGS}

CFLAGS = ${COMMON_FLAGS} -std=gnu99 -fPIC -Wall -Wextra `pkg-config --cflags gtk+-2.0 lilv-0` -I./include
LIBS = `pkg-config --libs gtk+-2.0 gmodule-export-2.0 lilv-0` -ldl

program = resorg
obj_files = main.o ui.o search-so.o ladspa/search.o dssi/search.o lv2/search.o

release: config
	make COMMON_FLAGS="${RELEASE_FLAGS}" ${program}

debug: config
	make COMMON_FLAGS="${DEBUG_FLAGS}" ${program}

${program}: ${obj_files}
	gcc ${LDFLAGS} -o ${program} ${obj_files} ${LIBS}

config: config.h

config.h: Makefile
	echo "#define DATADIR \""${DATADIR}"\"" > config.h

install: ${program}
	install -d ${DATADIR}/${pkg}
	install -t ${DATADIR}/${pkg} browser.ui
	install -t ${BINDIR} ${program}

	install -d ${DOCDIR}/${pkg}
	install -t ${DOCDIR}/${pkg} COPYING README TODO

uninstall:
	rm -rf ${BINDIR}/${program} ${DOCDIR}/${pkg} ${DATADIR}/${pkg}

clean:
	rm -rf ${program} ${obj_files}

.PHONY: clean uninstall release debug
