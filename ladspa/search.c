/*
 * This file is part of Resource Organizer.
 *
 * Copyright (C) 2014 Nikita Zlobin <nick87720z@gmail.com>
 *
 * Resource Organizer is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Resource Organizer is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resource Organizer.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <dirent.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <ladspa.h>

#include <resource.h>
#include <search-so.h>

static ResourceType rtype = {
	.label = "ladspa",
	.name = "LADSPA"
};

static ResourceHandlerFunc res_handler;

static void
describeLadspaPluginLibrary (const char * fileName)
{
	void * dl_Handle;

	if (strcmp (fileName + strlen (fileName) - strlen (".so"), ".so") != 0) return;

	// FIXME: Strange leaks in dlopen(), need valgrind self-check
	dl_Handle = dlopen (fileName, RTLD_NOW | RTLD_LOCAL);
	if (! dl_Handle) {
		fprintf (stderr, "dlerror: %s\n", dlerror());
		return;
	}

	/* This is a file and the file is a shared library! */
	LADSPA_Descriptor_Function descFunc;

	dlerror();
	descFunc = (LADSPA_Descriptor_Function) dlsym (dl_Handle, "ladspa_descriptor");
	if (dlerror() == NULL && descFunc) {
		const LADSPA_Descriptor * plugDesc;
		long lIndex = 0;

		Resource res = { .type = & rtype,
		                 .file = (char *)fileName,
		                 .url = NULL };
		size_t base_size = strlen (":/") + strlen (res.type->label) + strlen (fileName) + 1;

		// FIXME: Strange leaks in descFunc(), need valgrind self-check
		for( ; (plugDesc = descFunc (lIndex)) != NULL; lIndex++ )
		{
			size_t l_size, old_size = 0;
			char * url = NULL, * url_label;

			l_size = strlen (plugDesc->Label);
			if (l_size > old_size)
				url = realloc (url, base_size + l_size);

			if (url != res.url)
			{
				url_label = (res.url = url) + base_size - 1;
				sprintf (url, "%s:%s/", res.type->label, fileName);
			}
			sprintf (url_label, "%s", plugDesc->Label);

			res.name = (char *)plugDesc->Name;
			res.author = (char *)plugDesc->Maker;
			res_handler (& res);
		}
		free (res.url);
	}
	dlclose (dl_Handle);
}

void ladspaPathIterate (void)
{
	pathIterate ("LADSPA_PATH", describeLadspaPluginLibrary);
}

void ladspaResourceHandler (ResourceHandlerFunc cb_func)
{
	res_handler = cb_func;
}
