/*
 * This file is part of Resource Organizer.
 *
 * Copyright (C) 2014 Nikita Zlobin <nick87720z@gmail.com>
 *
 * Resource Organizer is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Resource Organizer is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resource Organizer.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __DSSI_SEARCH_H__
#define __DSSI_SEARCH_H__

typedef void (* FilePathHandlerFunc) (const char * filePath);

extern void
pathIterate (const char * pathEnv,
             FilePathHandlerFunc cb_func);

#endif
