/*
 * This file is part of Resource Organizer.
 *
 * Copyright (C) 2014 Nikita Zlobin <nick87720z@gmail.com>
 *
 * Resource Organizer is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Resource Organizer is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resource Organizer.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __RESOURCE_H__
#define __RESOURCE_H__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef
struct res_type {
	char * label;
	char * name;
} ResourceType;

typedef
struct resource {
	char * url; // Full plugin path, may differ between resource types
	char * name;
	char * file; // Path to file, containing resource
	char * author;
	char * email;
	char * homepage;
	ResourceType * type;
} Resource;

typedef void (* ResourceHandlerFunc) (Resource * r);

#define RESOURCE(p) ((Resource *) p)

static inline
void resourceUninit (Resource * r)
{
	free (r->url);
	free (r->name);
	free (r->file);
	free (r->author);
}

static inline
void resourceFree (Resource * r)
{
	resourceUninit (r);
	free (r);
}

#endif
